package com.violet.controller;

import com.violet.entities.CommonResult;
import com.violet.entities.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    public static HashMap<Long, Payment> hashmap = new HashMap<>();
    static {
        hashmap.put(1L, new Payment(1L, "AAAAAAAAAAAAAAAAAAAAA"));
        hashmap.put(2L, new Payment(2L, "BBBBBBBBBBBBBBBBBBBBB"));
        hashmap.put(3L, new Payment(3L, "CCCCCCCCCCCCCCCCCCCCC"));
    }

    @GetMapping("/payment/{id}")
    public CommonResult<Payment> paymentOpenFeign(@PathVariable("id") Long id) {
        Payment payment = hashmap.get(id);
        return new CommonResult<Payment>(200, "from mysql, serverPort: " + serverPort, payment);
    }
}
