package com.violet.controller;

import com.violet.service.StreamService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class StreamController {

    @Resource
    private StreamService service;

    @GetMapping("/sendMessage")
    public String send() {
        return service.send();
    }

}
