package com.violet.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class SentinelController {



    @GetMapping("getHotKey")
    @SentinelResource(value = "testHotKey", blockHandler = "deal_testHotKey")
    public String testHotKey(@RequestParam String p1, @RequestParam String p2) {
        int i = 1 / 0;
        return "---------testHotKey";
    }

    public String deal_testHotKey(String p1, String p2, BlockException blockException) {
        return "---------deal_HotKey-------/(ㄒoㄒ)/~~";
    }


    @GetMapping("/getA")
    public String getA() {
        System.out.println(Thread.currentThread().getName() + "\t" + "getAAAAAAAA");
        return Thread.currentThread().getName() + "\t" + "getAAAAAAAA";
    }

    @GetMapping("/getB")
    public String getB() {
        return "get--------BBBBBB------";
    }

    @GetMapping("/getD")
    public String getD() {
        try {
            System.out.println("getDDDDDDD ");
            int age = 10 / 0;
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "get--------BBBBBB------";
    }
}
