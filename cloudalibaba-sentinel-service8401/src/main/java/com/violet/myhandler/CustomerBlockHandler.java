package com.violet.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.violet.entities.CommonResult;

public class CustomerBlockHandler {

    public CommonResult handleException(BlockException exception) {
        return new CommonResult(444, exception.getClass().getCanonicalName() + "\t" + "11111服务不可用");
    }

    public CommonResult handleException2(BlockException exception) {
        return new CommonResult(444, exception.getClass().getCanonicalName() + "\t" + "22222服务不可用");
    }
}
