package com.violet.service.impl;

import com.violet.dao.OrderDao;
import com.violet.domain.Order;
import com.violet.service.AccountService;
import com.violet.service.OrderService;
import com.violet.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;

    @Resource
    private AccountService accountService;

    @Resource
    private StorageService storageService;

    @Override
    public void create(Order order) {
        log.info("开始新建订单");
        orderDao.create(order);

        log.info("------>订单微服务开始调用库存，做扣减");
        storageService.decrease(order.getProductId(), order.getCount());
        log.info("------>订单微服务开始调用账户余额，做扣减");

        accountService.decrease(order.getUserId(), order.getMoney());
        log.info("修改订单状态开始");
        orderDao.update(order.getUserId(), 0);
        log.info("修改订单状态结束");

        log.info("下订单结束了，(*^_^*)哈哈");
    }
}
