package com.violet.service;

import com.violet.domain.Order;

public interface OrderService {
    void create(Order order);
}
