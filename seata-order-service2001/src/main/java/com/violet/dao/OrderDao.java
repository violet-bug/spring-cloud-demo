package com.violet.dao;

import com.violet.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderDao {

    //新建订单
    void create(Order order);

    //修改状态
    void update(@Param("userId") Long userId, @Param("status") Integer status);


}
