package com.violet.service;

import com.violet.entities.CommonResult;
import com.violet.entities.Payment;
import org.springframework.stereotype.Component;

@Component
public class PaymentFallback implements PaymentService{
    @Override
    public CommonResult<Payment> paymentOpenFeign(Long id) {
        return new CommonResult<>(400, "服务降级返回，---PaymentFallbackService", new Payment(id, "error data"));
    }
}
