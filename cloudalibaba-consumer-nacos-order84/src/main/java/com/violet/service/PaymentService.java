package com.violet.service;

import com.violet.entities.CommonResult;
import com.violet.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "nacos-payment-provider", fallback = PaymentFallback.class)
public interface PaymentService {
    @GetMapping("/payment/{id}")
    public CommonResult<Payment> paymentOpenFeign(@PathVariable("id") Long id);
}
