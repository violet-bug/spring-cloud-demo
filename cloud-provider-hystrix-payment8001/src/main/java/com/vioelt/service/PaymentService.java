package com.vioelt.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class PaymentService {
    public String paymentInfo(Integer id) {
        return "线程池：" +Thread.currentThread().getName() + " paymentInfo_Ok, id=" + "\t" + "O(∩_∩)O";
    }

//    @HystrixCommand(fallbackMethod = "fail", commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000")
//    })
    @HystrixCommand
    public String paymentInfo_timeout(Integer id) {
        try {
            //int age = 1 / 0;
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "线程池：" +Thread.currentThread().getName() + " paymentInfo_Ok, id=" + "\t" + "/(ㄒoㄒ)/~~";
    }
}
