package com.violet.service.impl;

import com.violet.dao.AccountDao;
import com.violet.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountDao accountDao;

    @Override
    public void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money) {

       log.info("-------->account-service中扣减账户余额开始");
       //模拟超时异常，全局事务回滚
        try{
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException i) {i.printStackTrace();}
       accountDao.decrease(userId, money);
        log.info("-------->account-service中扣减账户余额结束");
    }
}
