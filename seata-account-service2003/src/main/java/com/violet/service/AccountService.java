package com.violet.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

public interface AccountService {
   void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);
}
