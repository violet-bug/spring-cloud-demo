package com.violet.service;

import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentHystrixService{

    @Override
    public String paymentInfo_Ok(Integer id) {
        return "ok " + id;
    }

    @Override
    public String paymentInfo_fail(Integer id) {
        return "fail " + id;
    }
}
