package com.violet.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {

    @Bean
    Logger.Level log() {
        return Logger.Level.FULL;
    }

}
